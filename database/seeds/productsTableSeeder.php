<?php

use Illuminate\Database\Seeder;
use App\product;

class productsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 60 ; $i++) {
            product::create([
                'title' => $faker->sentence(4),
                'slug' => $faker->slug,
                'subTitle' => $faker->sentence(5),
                'description' => $faker->text,
                'price' => $faker->numberBetween(10, 300)*100,
                'image' => 'https://via.placeholder.com/200x250'
            ]);
        }
    }
}
