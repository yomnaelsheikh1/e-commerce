<?php

namespace App\Http\Controllers;

use App\product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicate = Cart::search(function($cartItem , $rowId) use ($request) {
            return $cartItem->id == $request->product_id;
        });

        if($duplicate->isNotEmpty()) {
            return redirect()->route('products.index')->with('success', 'Product was already added to cart');
        }

        $product = product::find($request->product_id);

        Cart::add($product->id, $product->title, 1, $product->price)->associate('App\product');
        return redirect()->route('products.index')->with('success', 'Product was scuccessfully added to cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    // @param  int  $id
    public function destroy( )
    {
        Cart::destroy();
        return redirect()->route('products.index')->with('success', 'Cart is empty');
    }
}
