<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('/products', 'productsController');

Route::post('cart/add', 'CartController@store')->name('cart.store');

Route::get('cart/emptyCart', 'CartController@destroy')->name('cart.destroy');
Route::get('/cart', 'CartController@index')->name('cart.index');
